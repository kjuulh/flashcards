class Card:

    def __init__(self, question="", answer=""):
        self.question = question
        self.answer = answer
        self.status = "Unknown"

    def reveal(self):
        return {
            "question": f'{self.question}',
            "answer": f'{self.answer}',
            "status": f'{self.status}'
        }

    def __repr__(self):
        return {
            "question": f'{self.question}',
            "answer": f'{self.answer}',
            "status": f'{self.status}'
        }

    def set_question(self, question: str):
        self.question = question

    def set_answer(self, answer: str):
        self.answer = answer

    def set_status(self, status: str):
        if status == "Unknown" or status == "Known" or status == "Mastered":
            self.status = status
