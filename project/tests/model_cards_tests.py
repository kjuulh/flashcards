import unittest

from project.models.card import Card


class TestModels(unittest.TestCase):

    def test_initialize(self):
        card = Card(question="Question", answer="Answer")
        self.assertEqual(card.answer, "Answer")
        self.assertEqual(card.question, "Question")
        self.assertEqual(card.status, "Unknown")

    def test_status_promotion(self):
        card = Card()
        card.set_status("Known")
        self.assertEqual(card.status, "Known")

    def test_status_promotion_x2(self):
        card = Card()
        card.set_status("Mastered")
        self.assertEqual(card.status, "Mastered")

    def test_status_demotion(self):
        card = Card()
        card.set_status("Mastered")
        self.assertEqual(card.status, "Mastered")
        card.set_status("Known")
        self.assertEqual(card.status, "Known")

    def test_reveal(self):
        card = Card(question="Question", answer="Answer")

        response = {
            "question": "Question",
            "answer": "Answer",
            "status": "Unknown"
        }

        self.assertEqual(card.reveal(), response)


if __name__ == '__main__':
    unittest.main()
