import unittest

from controller.deck_controller import DeckController
from project.models.card import Card


class TestCase(unittest.TestCase):

    def test_controller_initializes(self):
        dc = DeckController()
        self.assertIsNotNone(dc)

    def test_controller_can_create_deck(self):
        dc = DeckController()
        dc.create("Deck")
        self.assertEqual(len(dc.decks), 1)

    def test_controller_can_delete_deck(self):
        dc = DeckController()
        dc.create("Deck")
        dc.delete("Deck")
        self.assertEqual(len(dc.decks), 0)

    def test_controller_can_load_deck(self):
        dc = DeckController()
        dc.create("Deck")
        deck = dc.load("Deck")
        self.assertEqual(deck.deck_name, "Deck")

    def test_controller_can_edit_deck(self):
        dc = DeckController()
        dc.create("Deck")
        deck = dc.load("Deck")
        deck.deck_name = "Deck2"
        self.assertEqual(deck.deck_name, "Deck2")

    def test_controller_can_list_decks(self):
        dc = DeckController()
        dc.create("Deck")
        dc.create("Deck2")

        deck = dc.load("Deck")
        deck.add(Card("Question: 1a", "Answer: 1a"))
        deck.add(Card("Question: 2a", "Answer: 2a"))

        deck2 = dc.load("Deck2")
        deck2.add(Card("Question: 1b", "Answer: 1b"))
        deck2.add(Card("Question: 2b", "Answer: 2b"))

        self.assertEqual(str(dc.decks[0].cards[0].question), "Question: 1a")
        self.assertEqual(str(dc.decks[0].cards[0].answer), "Answer: 1a")
        self.assertEqual(str(dc.decks[0].cards[1].question), "Question: 2a")
        self.assertEqual(str(dc.decks[0].cards[1].answer), "Answer: 2a")
        self.assertEqual(str(dc.decks[1].cards[0].question), "Question: 1b")
        self.assertEqual(str(dc.decks[1].cards[0].answer), "Answer: 1b")
        self.assertEqual(str(dc.decks[1].cards[1].question), "Question: 2b")
        self.assertEqual(str(dc.decks[1].cards[1].answer), "Answer: 2b")
