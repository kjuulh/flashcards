import unittest

from project.models.card import Card
from project.models.deck import Deck


class TestModels(unittest.TestCase):

    def test_add(self):
        deck = Deck()
        card1 = Card("Question1", "Answer1")
        card2 = Card("Question2", "Answer2")
        deck.add(card1)
        deck.add(card2)

        self.assertEqual(deck.cards[0].question, card1.question)
        self.assertEqual(deck.cards[1].question, card2.question)

    def test_remove_first(self):
        deck = Deck()
        card1 = Card("Question1", "Answer1")
        card2 = Card("Question2", "Answer2")
        deck.add(card1)
        deck.add(card2)

        self.assertEqual(deck.cards[0].question, card1.question)
        self.assertEqual(deck.cards[1].question, card2.question)

        deck.remove(card1)
        self.assertNotEqual(deck.cards[0].question, card1.question)
        self.assertEqual(deck.cards[0].question, card2.question)

    def test_remove_second(self):
        deck = Deck()
        card1 = Card("Question1", "Answer1")
        card2 = Card("Question2", "Answer2")
        deck.add(card1)
        deck.add(card2)

        self.assertEqual(len(deck.cards), 2)
        deck.remove(card2)
        self.assertEqual(len(deck.cards), 1)


if __name__ == '__main__':
    unittest.main()
